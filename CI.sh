#! /bin/bash
# set error handling
set -euo pipefail
#make sure g-cli is in path
die() { echo $1 >&2; exit 1; }
PATH=$PATH:/c/Program\ Files/G-CLI
#use backslashes for paths since G-CLI uses Windows Paths
#use Dummy to make sure LV closes before running anything
g-cli --kill "GCLI\Dummy.vi"
g-cli "GCLI\Run Test Suite.vi" -- "beforeswap.txt" || die "Test Suite Failed with "$?
g-cli "GCLI\Build.vi" || die "Build Failed with "$?
g-cli "GCLI\Swap In PPL.vi" || die "Swap Failed with "$?
g-cli --kill "GCLI\Run Test Suite.vi" -- "afterswap.txt" || die "Test Suite Failed with "$?
git restore Test/* || die "Problem Restoring Tests with "$?


